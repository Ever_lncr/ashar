from django.contrib import admin
from django.urls import path, include

from apps.accounts.views import schema_view


urlpatterns = [
    path('doc/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api/v1/', include('apps.tasks.urls')),
    path('api/v1/', include('apps.category.urls')),
    path('api/v1/', include('apps.accounts.urls')),
    path('api/v1/', include('apps.core.urls')),
]
