To run the developer version

If you have Ubuntu:

Install docker and docker-compose if you don't have them already
 https://docs.docker.com/engine/install/ubuntu/
 https://docs.docker.com/compose/install/

Go to a location where you want this project's code to be
Then open this location in termainal and type
1) git clone https://gitlab.com/Ever_lncr/ashar.git
2) Enter your authorization data
3) cd ashar/
4) docker compose up --build
