from django.db import models
from apps.accounts.models import Profile
from apps.category.models import Category


class Task(models.Model):
    name = models.CharField(max_length=128)
    summary = models.TextField()
    estimated_pay = models.IntegerField()
    date_time = models.DateTimeField()
    phone_number = models.CharField(max_length=10)
    host = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='task', blank=True, null=True)
    potential_executor = models.ManyToManyField(Profile, blank=True, related_name='potential_job')
    executor = models.ForeignKey(Profile, on_delete=models.SET_NULL, related_name='job', blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='task', blank=True, null=True)

    def __str__(self):
        return self.name
