from django.conf import settings
from rest_framework import serializers

from .models import Task


class TaskSerializer(serializers.ModelSerializer):
    date_time = serializers.DateTimeField(format=settings.DATETIME_FORMAT)

    class Meta:
        model = Task
        fields = ('id', 'name', 'summary', 'estimated_pay', 'phone_number', 'date_time', 'category')


class TaskAcceptSerializer(serializers.Serializer):
    task_id = serializers.IntegerField()


class TaskApproveSerializer(serializers.Serializer):
    task_id = serializers.IntegerField()
    executor_id = serializers.IntegerField()
