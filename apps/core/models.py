from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.accounts.models import Profile

CHOICES = (
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5)
)


class Rating(models.Model):
    sender = models.ForeignKey(Profile, on_delete=models.SET_NULL, related_name='sent_rating', blank=True, null=True)
    recepient = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='receieved_rating')
    mark = models.IntegerField(choices=CHOICES)

    def __str__(self):
        return self.sender.user.username + ' ' + str(self.mark)


@receiver(post_save, sender=Rating)
def set_mentor_mark(sender, instance, created, **kwargs):
    recepient = instance.recepient
    rating = recepient.rating.all()
    amount = rating.count()
    sum = 0
    for i in rating:
        sum += i.mark
    recepient.mark = sum / amount
    recepient.save()
