from rest_framework import serializers
from .models import Rating


class RatingSerializer(serializers.Serializer):
    profile_id = serializers.IntegerField()
    mark = serializers.IntegerField(min_value=0, max_value=5)

    def validate_mentor_id(self, value):
        if not Profile.objects.filter(id=value).exists():
            raise serializers.ValidationError(f'Profile with id: {value} is not found')
        return value
