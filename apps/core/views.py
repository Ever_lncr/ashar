from rest_framework import viewsets, views, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema

from apps.accounts.models import Profile
from .serializers import RatingSerializer


class RatingView(views.APIView):

    @swagger_auto_schema(request_body=RatingSerializer,
                         operation_description='Evaluates given profile',
                         responses={200: 'The mark is given', 400: 'Bad request'})
    def post(self, request):
        user = request.user

        if not hasattr(user, 'profile'):
            return response.Response(
                {'detail': ['You don\'t have a profile yet']},
                status=status.HTTP_404_NOT_FOUND,
            )
        profile = user.profile

        if serializer.is_valid():
            data = serializer.validated_data
            instance = Rating.objects.filter(
                sender=profile, recepient=Profile.objects.get(id=data['profile_id'])).first()
            if instance:
                instance.mark = data['mark']
                instance.save()
                return response.Response({"The mark is updated"}, status=status.HTTP_200_OK)
            instance = Rating.objects.create(sender=profile,
                                             recepient=Profile.objects.get(id=data['profile_id']),
                                             mark=data['mark'])
            return response.Response({"The new mark is given"}, status=status.HTTP_200_OK)
        return response.Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
