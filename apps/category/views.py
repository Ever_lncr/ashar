from rest_framework import viewsets
from .permissions import IsAdminUserOrReadOnly
from .models import Category
from .serializers import CategorySerializer


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    lookup_field = 'id'
    queryset = Category.objects.all()
    permission_classes = (IsAdminUserOrReadOnly,)
